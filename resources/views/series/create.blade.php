<x-layout title="Nova série">
    {{-- <x-series.form :action="route('series.store')" :nome="old('nome')" :update="false" /> --}}
    <form action="{{ route('series.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col-8">
                <label class="form-label" for="nome">Nome:</label>
                <input autofocus class="form-control" type="text" name="nome" id="nome" value="{{ old('nome') }}">
            </div>
            <div class="col-2">
                <label class="form-label" for="seasonsQty">Nº de temporadas:</label>
                <input class="form-control" type="text" name="seasonsQty" id="seasonsQty" value="{{ old('seasonsQty') }}">
            </div>
            <div class="col-2">
                <label class="form-label" for="episodesPerSeason">Eps/temporada:</label>
                <input class="form-control" type="text" name="episodesPerSeason" id="episodesPerseason" value="{{ old('episodesPerseason') }}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-12">
                <label for="cover" class="form-label">Capa</label>
                <input type="file" name="cover" id="cover" class="form-control" accept="image/gif, image/jpeg, image/png">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </form>
</x-layout>
